import {useState,useEffect} from 'react'
import ProductCard from '../components/ProductCard';


export default function Products(){


	const [products, setProducts] = useState([]);

	useEffect(()=> {
		fetch('https://serene-springs-89901.herokuapp.com/products')
		.then(res=> res.json())
		.then(data=>{
			//console.log(data);

			setProducts(data.map(product=>{
				return(
					<ProductCard key={product._id} productProp = {product}/>
					)
			}))
		})

	},[]);
	return(
		<>
		<h1>Available Products:</h1>
		{products}
		</>
		)
}
