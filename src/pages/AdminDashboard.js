import { useContext, useState, useParams,useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AdminPage() {
  //const { productId } = useParams();
  const { user } = useContext(UserContext);
  const [allProducts, setAllProducts] = useState([]);

  const getData = () => {
    fetch(`https://serene-springs-89901.herokuapp.com/products`, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setAllProducts(
          data.map((product) => {
            return (
              <tr key={product._id}>
                <td>{product._id}</td>
                <td>{product.name}</td>
                <td>{product.description}</td>
                <td>{product.price}</td>
                <td>{product.quantity}</td>
                <td>{product.isActive ? 'Active' : 'Inactive'}</td>
                <td>
                  {product.isActive ===true? (
                    <Button
                      variant="muted"
                      onClick={() => archive(product._id, product.name)}
                    >
                      Archive
                    </Button>
                  ) : (
                    <>
                      <Button
                        variant="Primary"
                        onClick={() => activate(product._id, product.name)}
                      >
                        Activate
                      </Button>
                      <Button
                        variant="secondary"
                        onClick={() => archive(product._id)}
                      >
                        Delete
                      </Button>
                      <Button as={Link} to={`/updateProduct/${product._id}`}>
                        Edit
                      </Button>
                    </>
                  )}
                </td>
              </tr>
            );
          })
        );
      });
  };


  //archiving product
  const archive = (productId, productName) => {
    console.log(productId);
    console.log(productName);

    fetch(`https://serene-springs-89901.herokuapp.com/products/archiveProduct/${productId}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        isActive: false,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Archive Succesful!',
            icon: 'success',
            text: `${productName} is now inactive.`,
          });
          getData();
        } else {
          Swal.fire({
            title: 'Archive Unsuccessful!',
            icon: 'error',
            text: `Something went wrong. Please try again later!`,
          });
        }
      });
  };

  //Making the product active
  const activate = (productId, productName) => {
    console.log(productId);
    console.log(productName);

    fetch(`https://serene-springs-89901.herokuapp.com/products/activateProduct/${productId}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        isActive: true,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data) {
          Swal.fire({
            title: 'Activate Succesful!',
            icon: 'success',
            text: `${productName} is now active.`,
          });
          getData();
        } else {
          Swal.fire({
            title: 'Activate Unsuccessful!',
            icon: 'error',
            text: `Something went wrong. Please try again later!`,
          });
        }
      });
  };

  useEffect(() => {
    getData();
  }, []);

  return user.isAdmin ? (
    <>
      <div className="mt-5 mb-3 text-center">
        <h1>Admin Dashboard</h1>
        {/*A button to add a new product*/}
        <Button as={Link} to="/addProduct" variant="secondary">
          Add Product
        </Button>
        <Button as={Link} to="/products" variant="secondary">
          Products
        </Button>
        <Button as={Link} to="/OrderView" variant="secondary">
          Orders
        </Button>
      </div>
      <Table striped bordered hover >
        <thead>
          <tr>
            <th>Product ID</th>
            <th>Product Name</th>
            <th>Description</th>
            <th>Price</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>{allProducts}</tbody>
      </Table>
    </>
  ) : (
    <Navigate to="/AdminDashboard" />
  );
}
