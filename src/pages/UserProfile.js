import {useState, useEffect} from 'react'
import {Container, Table} from 'react-bootstrap'


export default function Profile() {

    const [userDetails, setUserDetails] = useState({
        firstName: "",
        lastName: "",
        email: "",
        mobileNo: "",
        isAdmin:"",
        order:""

    })

    useEffect(()=>{
    fetch('https://serene-springs-89901.herokuapp.com/users/getUserDetails',{
      headers: {
      	'Content-Type':'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
    })
    .then(res => res.json())
    .then(data=> {
      //console.log(data);
      setUserDetails({
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email,
          mobileNo: data.mobileNo,
          isAdmin: data.isAdmin,
          orders: data.orders
      })
      })
    },[])

console.log(Profile)

    return(
        <div>
        <Container className="pt-5">
            <Table>
              <thead>
                <tr>
                  <th colSpan={4} className="text-center">Profile</th>

                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>First Name</td>
                  <td>{userDetails.firstName}</td>

                </tr>
                <tr>
                  <td>Last Name</td>
                  <td>{userDetails.lastName}</td>
                </tr>

                <tr>
                  <td>Email</td>
                  <td>{userDetails.email}</td>
                </tr>

                <tr>
                  <td>Mobile No.</td>
                  <td>{userDetails.mobileNo}</td>
                </tr>
                <tr>
                  <td>My purchases:</td>
                  <td>{userDetails.orders}</td>
                </tr>
              

              </tbody>
            </Table>
         </Container>

        </div>
    )
}