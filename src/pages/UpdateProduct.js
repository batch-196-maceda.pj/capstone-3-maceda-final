import { useContext, useEffect, useState } from 'react';
import { useParams,  Navigate } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import UserContext from "../UserContext";

export default function UpdateUser() {
    const { productId } = useParams();

    const {user} = useContext(UserContext);
    const [name, setName] = useState();
    const [description, setDescription] = useState();
    const [price, setPrice] = useState();

    // const navigate = useNavigate();
    
    useEffect(() => {
        fetch(`https://serene-springs-89901.herokuapp.com/products/getSingleProduct/${productId}`)
        .then((response) => response.json())
        .then(data => {
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);

            console.log(data);  
        })
    }, [productId]);



    const updateProduct = async () => {
        fetch(`hhttps://serene-springs-89901.herokuapp.com/products/updateProduct/${productId}`,{
            method:'PUT',
            body:JSON.stringify(
                {name:name,description:description,price:price}
            ),
            headers:{
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        });
       
        
    }

    return (
        (user.isAdmin)
        ?
        <>
        <Form onSubmit={event => updateProduct(event)} >
        <Form.Group controlId="name">
                <Form.Label>Product Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Product name" 
                    required 
                    value={name}
                    onChange={e => setName(e.target.value)}
                />
            </Form.Group>
            <Form.Group controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Description" 
                    required 
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                />
            </Form.Group>
            <Form.Group controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control 
                    type="number" 
                    placeholder="Price" 
                    required 
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                />
            </Form.Group>
            <Button type="submit">Update</Button>
        </Form>
        </>
        :
        <Navigate to="/AdminDashboard" />
    );
}
