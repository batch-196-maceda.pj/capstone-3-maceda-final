import { useState } from "react"
import { ElementDescription } from "./CommonProp"
import {CustomSpinnerSmall} from "./Spinner"


export default function ProductOrder({productsProp}){
    const{productId, quantity} = productsProp
    const[productName, setProductName] = useState("")
    const [isLoading, setIsLoading] = useState(true)

    fetch(`https://serene-springs-89901.herokuapp.com/products/${productId}`
            ).then(res=>res.json())
            .then(data => {
                    setIsLoading(false)
                    setProductName(data.name)
            }).catch(err => console.log(err))

    return(
        isLoading ?
        <CustomSpinnerSmall></CustomSpinnerSmall>
        :
        <ElementDescription>
                {productName} (Quantity: {quantity})
        </ElementDescription>
    )

}