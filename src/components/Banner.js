import{Row, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner(){
	return(

		<Row>
			<Col className="p-5">
				<h1>Farmers Market</h1>
				<p>Freshly picked Quality Fruits and Vegies</p>

				<h2>About</h2>
				<p>
					<strong>Farmers Market</strong> is an e commerce website that provide you with freshly picked fruits and vegetables. 
					We vouch that all our product is naturaly grown and naturally maintained with minimal to no chemical maintenance. To ensure 
					that what we give an sell to our consumers are healthy and natural. Come, look for your vegie and fruity needs with us! <i>Welcome to Farmers Market</i>!   
				</p>
				<Button variant="primary" as={Link} to="/products">Shop now!</Button>
			</Col>

		</Row>

		)
};