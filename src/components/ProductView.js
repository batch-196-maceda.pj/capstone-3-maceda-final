import{Container, Row, Col, Card, Button} from 'react-bootstrap';
import{useState,useEffect,useContext} from 'react';
import{useParams, useNavigate,Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function ProductView(){
	const{user}=useContext(UserContext);
	const history = useNavigate();
	const {productId} = useParams();


	const[name,setName]=useState("");
	const [description, setDescription] = useState("");
	const[price,setPrice]= useState(0);
	const[quantity,setQuantity]=useState(0);
	// const[totalAmount,updateTotalAmount]=useState(0);
	const products=()=>{
		fetch (`https://serene-springs-89901.herokuapp.com/products`,{})
	}


	const buyNow = (productId) =>{
		fetch (`https://serene-springs-89901.herokuapp.com/orders/createOrder/${productId}`,{
			method:'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization:`Bearer ${localStorage.getItem('token')}`,
				"Access-Control-Allow-Origin":"*",
				"Access-Control-Allow-Credentials":true,
				"status":200
			},
			body:JSON.stringify({
				productId: productId,
				quantity: quantity,
				setPrice:price
			})
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);


		if(data){
			Swal.fire({
				title:'Successfully Purchased!',
				icon:'success',
				text:'Thank you for Purchasing!'
			});

			history("/products");
		}else{
			Swal.fire({
				title:'Something went wrong!',
				icon:'error',
				text: 'Please try again later'
			})
		}
		})
	};

	useEffect(()=>{
		console.log(productId)
		fetch(`https://serene-springs-89901.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data=>{
			//console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
		})
	},[productId]);

	return(
		<Container className ="mt-5">
			<Row>
				<Col lg={{span:6, offset: 3}}>
					<Card>
						<Card.Title>Product:{name}</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>Php {price}</Card.Text>
						
						{user.id !== null? 
							<Button variant="primary" onClick={()=>buyNow(productId)}>Buy now!</Button>
							:
							<Link className="btn btn-danger" to="/login">Log In</Link>
						}
						
						
						
					</Card>
				</Col>
			</Row>

		</Container>

		)
}